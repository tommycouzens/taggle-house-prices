This algorithm tries to find the best features for the model. It is based on the idea of natural selection, where you randomly create a list of attributes and then test them and keep the best that survive, analogous to how animals evolved their genetics.  

First you create a random population with the function ```generateFirstPopulation```. In this case each member of the population is a list of different features. So the population is just a list of lists.  

After the first generation there is four steps repeated:

![](https://quantdare.com/wp-content/uploads/2016/12/lakalgorithm.jpg)


1. Fitness - Determine the fitness of the population by calcuating the mean absolute error (mae) for each feature list in the population.  

2. Selection - Only keep the feature lists which produce the lowest maes  

3. Reproduce - Combine the existing ones to make new feature lists (randomly selecting which features are combined out of the two being combined to use)  

4. Mutate - Any feature has a slight chance of randomly changing to another feature. This is to avoid converging to local minima, and is the driver of discovering new useful combinations. 

These four steps happen through the four functions:
```computePerformance```  
```selectFromPopulation```  
```createChildren```  
```mutatePopulation```

![](https://www.easycalculation.com/maths-dictionary/images/local_minimum.png)


The function ```mutlipleGenerations``` is the actual genetic alogorithm with the following parameters:  

**Iterations**  
The number of generations of the genetic algorithm. Increasing should improve results but take longer.  
**populationSize**  
The total population pool at any given time. Increasing should improve results but take longer, but I found there to be little difference between 10 and 100  
**featureListLength**  
How many features each member of the population contains.  
**best_sample**  
How many are selected out of the population after being fitness tested  
**lucky_few**  
In order to avoid local minima and stagnation, this is the number a number of 'weak' members of the population that are chosen to survive and breed.  
**chanceOfMutation**  
The chance that on any feature list that it will have one feature mutate (randomly chance to another feature).